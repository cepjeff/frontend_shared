
    
    $('#wizard').smartWizard({
        onLeaveStep:leaveAStepCallback,
        onFinish:onFinishCallback,
        //enableFinishButton:true    
    });

    function leaveAStepCallback(obj, context){
        var step_num= obj.attr('rel');
        return validateSteps(step_num);
    }

    function onFinishCallback(objs, context){
        if(validateAllSteps()){
            alert("success");
            $('.buttonFinish').prop('disabled', false);
            $form = $(this); //wrap this in jQuery
            $.ajax({
                url         : $("#form_wz_act").attr('action'),
                type        : "POST",
                data        : $("#form_wz_act").serialize(),
                beforeSend  : function(){
                    $(".buttonFinish").text("Loading");
                    $('.buttonFinish').addClass('buttonDisabled');
                    $('.buttonNext').addClass('buttonDisabled');
                    $('.buttonPrevious').addClass('buttonDisabled');
                },
                success:function(data){
                    $(".buttonFinish").text("Finish");
                    $('.buttonFinish').prop('disabled', false); 
                    $("p#message").html('<div role="alert" class="alert alert-success alert-dismissible fade in">'+
                                        '<button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>'+
                                        '<strong>Updated data Success!</strong></div>');
                    $("html, body").animate({ scrollTop: 0 }, "slow");
                    $("#form_wz_act")[0].reset();
                    setTimeout(function(){
                        window.location.reload();
                    }, 2 * 1000);
                    
                },
                error: function(){
                    $("#msg").slideDown();
                    $('.buttonFinish').prop('disabled', true);
                }
            });
            //$('#form_wz_act').submit();
        }
    }
    // Your Step validation logic
    function validateSteps(stepnumber){
        var isStepValid = true;
        // validate step 1
        if(stepnumber == 1){
            // Your step validation logic
            // set isStepValid = false if has errors
        }
        return isStepValid;      
    }
    function validateAllSteps(){
        var isStepValid = true;
        // all step validation logic     
        
        return isStepValid;
    }
        $('.buttonNext').addClass('btn btn-success');
        $('.buttonPrevious').addClass('btn btn-primary');
        $('.buttonFinish').addClass('btn btn-default');


        // initialize the validator function
    validator.message.date = 'not a real date';

      // validate a field on "blur" event, a 'select' on 'change' event & a '.reuired' classed multifield on 'keyup':
      $('form')
        .on('blur', 'input[required], input.optional, select.required', validator.checkField)
        .on('change', 'select.required', validator.checkField)
        .on('keypress', 'input[required][pattern]', validator.keypress);

      $('.multi.required').on('keyup blur', 'input', function() {
        validator.checkField.apply($(this).siblings().last()[0]);
      });

      $('form').submit(function(e) {
        e.preventDefault();
        var submit = true;

        // evaluate the form using generic validaing
        if (!validator.checkAll($(this))) {
          submit = false;

        }

        if (submit){
            $('#send').prop('disabled', false);
            $form = $(this); //wrap this in jQuery
            $.ajax({
                url         : $form.attr('action'),
                type        : "POST",
                data        : $("form").serialize(),
                beforeSend  : function(){
                    $("#send").text("Loading");
                    $('#send').prop('disabled', true);
                },
                success:function(data){
                    $("#send").text("submit");
                    $('#send').prop('disabled', false); 
                    $("p#message").html('<div role="alert" class="alert alert-success alert-dismissible fade in">'+
                                        '<button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>'+
                                        '<strong>Updated data Success!</strong></div>');
                    $("html, body").animate({ scrollTop: 0 }, "slow");
                    $($form)[0].reset();
                    setTimeout(function(){
                        window.location.reload();
                    }, 1 * 1000);
                },
                error: function(){
                    $("#msg").slideDown();
                    $('#send').prop('disabled', true);
                }
            });
          //this.submit();
        }

        return false;
    });
$('.date-picker').daterangepicker({
    format: 'YYYY-MM-DD',
    singleDatePicker: true,
    //calender_style: "picker_4",
    showDropdowns: true,
    }, function(start, end, label) {
        //console.log(start.toISOString(), end.toISOString(), label);
});