$(document).ready(function() {
    $('.tree').treegrid({
      expanderExpandedClass: 'glyphicon glyphicon-minus',
      expanderCollapsedClass: 'glyphicon glyphicon-plus'
    });
    var table_load_employee = $("#datatable_employee").DataTable({
        "bProcessing": true,
        "bServerSide": true,
        "sAjaxSource": $("#datatable_employee").data("uri"),
        "sServerMethod": "POST",        
        "aaSorting": [[1, "asc"]],
        "iDisplayLength": 10,
        "aoColumns" : [                                    
            {"mData": "nik",
                "mRender" : function ( data, type, rows ) {             
                    return '<a  title="View" href="employee/view/'+rows.code+'">'+rows.nik+'</a>';
                }
            },
            {"mData": "email"},
            {"mData": "first_name"},
            {"mData": "last_name"},
            {"mData": "gender",
                "mRender" : function ( data, type, rows ) {             
                    var gender;
                    if(rows.gender == 1){gender = "Laki - Laki"}else{gender = "Perempuan"}
                    return gender;
                }
            },
            {"mData": "employee_status",
                "mRender" : function ( data, type, rows ) {             
                    var gender;
                    if(rows.employee_status == 1){
                      employee_status = "Full Time";
                    }else if(rows.employee_status == 2){
                      employee_status = "Contract";
                    }else if(rows.employee_status == 3){
                      employee_status = "Training";
                    }else if(rows.employee_status == 4){
                      employee_status = "Probation";
                    }else{
                      employee_status = "Perempuan"
                    }
                    return employee_status;
                }

            },
            {"mData": "show",
                "mRender" : function ( data, type, full ) {             
                    var link = '<a href="../execute/delete/tr_employee/id/'+full.id+'" onclick="return false" id="del_employee" title="Delete" class="btn btn-danger btn-xs" data-id='+full.id+' ><i class="fa fa-trash-o"></i></a>';
                    return link;
               }
            },
        ]
    }); 
    var table_user = $("#datatable_user").DataTable();
    var table_role = $("#datatable_role").DataTable();
    var table_menu = $("#datatable_menu").DataTable({
      "aaSorting": [[6, "asc"]],
    });
    var datatable  = $("#datatable").DataTable({
      //"aaSorting": [[3, "asc"]],
    });
        var handleDataTableButtons = function() {
          if ($("#datatables").length) {
            $("#datatables").DataTable({
              
              dom: "Bfrtip",
              buttons: [
                {
                  extend: "copy",
                  className: "btn-sm"
                },
                {
                  extend: "csv",
                  className: "btn-sm"
                },
                {
                  extend: "excel",
                  className: "btn-sm"
                },
                {
                  extend: "pdfHtml5",
                  className: "btn-sm"
                },
                {
                  extend: "print",
                  className: "btn-sm"
                },
              ],
              responsive: true
            });
          }
        };

        TableManageButtons = function() {
          "use strict";
          return {
            init: function() {
              handleDataTableButtons();
            }
          };
        }();

        //$('#datatable').dataTable();
        $('#datatable-keytable').DataTable({
          keys: true
        });

        $('#datatable-responsive').DataTable();

        $('#datatable-scroller').DataTable({
          ajax: "js/datatables/json/scroller-demo.json",
          deferRender: true,
          scrollY: 380,
          scrollCollapse: true,
          scroller: true
        });

        var table = $('#datatable-fixed-header').DataTable({
          fixedHeader: true
        });

        TableManageButtons.init();
        $('#datatable_role tbody').on( 'click', '#delete_role', function () {
          if(confirm("Are You Sure Delete This Record ?..")){
            $.post( $(this).attr("href"), function( data ) {
              $( ".result" ).html( data );
              new PNotify({
                    title: 'Success',
                    text: "Delete Success",
                    type: 'success',
                    styling: 'bootstrap3'
                });  
            });
            table_role
            .row( $(this).parents('tr') )
            .remove()
            .draw();
          }else{
            return false;  
          }
          } );  
        $('#datatable_user tbody').on( 'click', '#delete_user', function () {
          if(confirm("Are You Sure Delete This Record ?..")){
            
            $.post( $(this).attr("href"), function( data ) {
               new PNotify({
                    title: 'Success',
                    text: "Delete Success",
                    type: 'success',
                    styling: 'bootstrap3'
                });  
            });
             table_user
              .row( $(this).parents('tr') )
              .remove()
              .draw();
          }else{
            return false;  
          }
        });  
        $('#datatable_menu tbody').on( 'click', '#delete_menu', function () {
          if(confirm("Are You Sure Delete This Record ?..")){
        
            $.post( $(this).attr("href"), function( data ) {
               new PNotify({
                    title: 'Success',
                    text: "Delete Success",
                    type: 'success',
                    styling: 'bootstrap3'
                });  
            });
             table_menu
              .row( $(this).parents('tr') )
              .remove()
              .draw();
          }else{
            return false;  
          }
        }); 
        $('#datatable_organization tbody').on( 'click', '#delete_organization', function () {
          if(confirm("Are You Sure Delete This Record ?..")){
        
            $.post( $(this).attr("href"), function( data ) {
               setTimeout(function(){
                        window.location.reload();
                    }, 1 * 1000);
               new PNotify({
                    title: 'Success',
                    text: "Delete Success",
                    type: 'success',
                    styling: 'bootstrap3'
                });  
            });
             
               
          }else{
            return false;  
          }
        });
        $('#datatable_position tbody').on( 'click', '#delete_position', function () {
          if(confirm("Are You Sure Delete This Record ?..")){
        
            $.post( $(this).attr("href"), function( data ) {
               setTimeout(function(){
                        window.location.reload();
                    }, 1 * 1000);
               new PNotify({
                    title: 'Success',
                    text: "Delete Success",
                    type: 'success',
                    styling: 'bootstrap3'
                });    
            });
             
               
          }else{
            return false;  
          }
        });
        $('#datatable_payroll_component tbody').on( 'click', '#delete_payroll_component', function () {
          if(confirm("Are You Sure Delete This Record ?..")){
        
            $.post( $(this).attr("href"), function( data ) {
               setTimeout(function(){
                        window.location.reload();
                    }, 1 * 1000);
               new PNotify({
                    title: 'Success',
                    text: "Delete Success",
                    type: 'success',
                    styling: 'bootstrap3'
                });  
            });
             
               
          }else{
            return false;  
          }
        });
        $('#datatable_employee tbody').on( 'click', '#del_employee', function () {
          if(confirm("Are You Sure Delete This Record ?..")){
        
            $.post( $(this).attr("href"), function( data ) {
               $("p#message").html('<div role="alert" class="alert alert-success alert-dismissible fade in">'+
                                        '<button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>'+
                                        '<strong>Delete data Success!</strong></div>');
               setTimeout(function(){

                        window.location.reload();
                    }, 1 * 1000);
               new PNotify({
                    title: 'Success',
                    text: "Delete Success",
                    type: 'success',
                    styling: 'bootstrap3'
                });  
            });
             
               
          }else{
            return false;  
          }
        });  
         
      });

function update_position_amount(btn){
    if(confirm("Are You Sure Update This Record ?..")){
        var idx = $(btn).data('id');
           
             $.ajax({
                url         : $(btn).attr("href"),
                type        : "POST",
                data        : "amount=" + $("#amount-"+idx).val(),
                beforeSend  : function(){
                    $("#update_position_amount-"+idx).text("Loading");
                    $('#update_position_amount-'+idx).prop('disabled', true);
                },
                success:function(data){
                    $("#update_position_amount-"+idx).text("Save");
                    $('#update_position_amount-'+idx).prop('disabled', false); 
                    
                   new PNotify({
                    title: 'Success',
                    text: "Update Success",
                    type: 'success',
                    styling: 'bootstrap3'
                });  
                },
                error: function(){
                    /*$("#msg").slideDown();
                    $('#send').prop('disabled', true);*/
                }
            });
               //return false; 
          }else{
            return false;  
          }
}
function choise_payrol_component(data){
    $.ajax({
        url         : $(data).attr("data-uri"),
        type        : "POST",
        dataType: "html",
                data        : "company_id=" + $(data).val(),
                beforeSend  : function(){
                    $("#loading").html("Loading...");
                },
                success:function(data){
                    $("#loading").html("");
                    $("#loads").html(data);

                },
                error: function(){
                    /*$("#msg").slideDown();
                    $('#send').prop('disabled', true);*/
                }
            });
}