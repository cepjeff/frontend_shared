function shows_list(data){

     $.ajax({
        url         : $("#send").data('url'),
        type        : "POST",
        dataType    : "html",
        data        : $("#form_show_employee").serialize(),
        beforeSend  : function(){
            $("#send").text("Loading");
            $('#send').prop('disabled', true);
        },
        success:function(data){
            $("#send").text("Run");
            $('#send').prop('disabled', false); 
            $("#datatable_show_employee").html(data);
        },
        error: function(xhr, status, error){
           new PNotify({
                    title: 'System Error',
                    text: xhr.responseText,
                    type: 'error',
                    styling: 'bootstrap3'
                });
        }
    });
}

function shows_slip(data){

     $.ajax({
        url         : $("#send").data('url'),
        type        : "POST",
        dataType    : "html",
        data        : $("#form_show_employee_slip").serialize(),
        beforeSend  : function(){
            $("#send").text("Loading");
            $("#data_show_employee_show").html("Loading..");
            $('#send').prop('disabled', true);
        },
        success:function(data){
            $("#send").text("Show");
            $('#send').prop('disabled', false); 
            $("#data_show_employee_show").html(data);
        },
        error: function(xhr, status, error){
             $("#send").text("Show");
            $('#send').prop('disabled', false); 
            new PNotify({
                    title: 'System Bussy',
                    text: xhr.responseText,
                    type: 'error',
                    styling: 'bootstrap3'
                });
        }
    });
}

